/* jshint node: true */
'use strict';

var CouchLogin = require('couch-login');
var out = require('out');
var npmconf = require('npmconf');
var user = process.env.NPM_USER;
var pass = process.env.NPM_PASS;
var couch;

/**
  # npm-authenticate

  This is a simple npm login helper that works when the standard `npm adduser`
  command fails (as it was for me after the recent npm outage).  It uses the
  same packages that are used in [npm](https://github.com/isaacs/npm) and
  [npm-registry-client](https://github.com/isaacs/npm-registry-client) to do
  a login and write the required information to your local `~/.npmrc` file so
  you can get back up and publishing.

  ## Example Usage

  After installing, you should be able to run like so:

  ```
  NPM_USER=<username> NPM_PASS=<pass> npm-authenticate
  ```

  All being well, your `.npmrc` file will be updated with valid `_session`,
  `_auth`, `username` and `email` keys ready for publishing...
**/

module.exports = function() {
  if (! user) {
    return out.error('No user specified (expected NPM_USER)');
  }

  if (! pass) {
    return out.error('No password specified (expected NPM_PASS)');
  }

  // connect to couch
  couch = new CouchLogin('http://registry.npmjs.org/');

  // login
  couch.login({ name: user, password: pass }, function(err, res, data) {
    if (err) {
      return out.error(err);
    }

    if (data && data.error) {
      return out.error(data.reason);
    }

    // get my details
    getUserDetails(function(err, res, data) {
      if (err || data.error) {
        return out.error('could not get details');
      }

      // load the config and then update
      npmconf.load(function(err, conf) {
        if (err) {
          return out.error('Unable to open ~/.npmrc');
        }

        conf.set('username', user, 'user');
        conf.set('email', data.email, 'user');
        conf.set('_auth', new Buffer(user + ':' + pass).toString('base64'), 'user');
        conf.set('_token', couch.token, 'user');
        conf.save('user', function(err) {
          if (err) {
            return out.error(err);
          }

          out('Updated npm configuration');
        });
      });
    });
  });
};

function getUserDetails(callback) {
  couch.get('/_users/org.couchdb.user:' + user, callback);
}