# npm-authenticate

This is a simple npm login helper that works when the standard `npm adduser`
command fails (as it was for me after the recent npm outage).  It uses the
same packages that are used in [npm](https://github.com/isaacs/npm) and
[npm-registry-client](https://github.com/isaacs/npm-registry-client) to do
a login and write the required information to your local `~/.npmrc` file so
you can get back up and publishing.


[![NPM](https://nodei.co/npm/npm-authenticate.png)](https://nodei.co/npm/npm-authenticate/)


## Example Usage

After installing, you should be able to run like so:

```
NPM_USER=<username> NPM_PASS=<pass> npm-authenticate
```

All being well, your `.npmrc` file will be updated with valid `_session`,
`_auth`, `username` and `email` keys ready for publishing...

## License(s)

### MIT

Copyright (c) 2013 Damon Oehlman <damon.oehlman@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
